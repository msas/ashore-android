package com.trutek.looped.ui.chats.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.trutek.looped.R;
import com.trutek.looped.chatmodule.data.contracts.models.DialogModel;
import com.trutek.looped.chatmodule.data.helper.DataManager;
import com.trutek.looped.chatmodule.utils.DbUtils;
import com.trutek.looped.data.contracts.models.ConnectionModel;
import com.trutek.looped.data.contracts.services.IConnectionService;
import com.trutek.looped.msas.common.Utils.Constants;
import com.trutek.looped.msas.common.contracts.OnActionListener;
import com.trutek.looped.msas.common.views.maskedimageview.MaskedImageView;
import com.trutek.looped.utils.DateUtils;
import com.trutek.looped.utils.helpers.SharedHelper;
import com.trutek.looped.utils.image.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DialogsAdapter extends RecyclerView.Adapter<DialogsAdapter.ViewHolder> {


    private static final String TAG = DialogsAdapter.class.getSimpleName();
    private Context context;
    private List<DialogModel> dialogs;
    private OnActionListener<DialogModel> onLongPress;
    private List<ConnectionModel> connections;
    DataManager dataManager;

    public DialogsAdapter(Context context, List<DialogModel> dialogs, List<ConnectionModel> connections, OnActionListener<DialogModel> onLongPress) {
        this.context = context;
        this.dialogs = dialogs;
        this.connections = connections;
        this.onLongPress = onLongPress;
        dataManager = DataManager.getInstance();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_dialogs, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final DialogModel model = dialogs.get(holder.getAdapterPosition());
        boolean is_Connection = false;
        int i = -1;
        ConnectionModel user_model = null;
        holder.lastMessage.setText(model.getLastMessage());
        

        if(model.type.equals("PRIVATE")){
            for (ConnectionModel connectionModel: connections) {
                if(model.dialogUsersId.contains(String.valueOf(connectionModel.profile.chat.id))){
                    user_model = connectionModel;
                    break;
                }
                user_model = null;
            }
        }
        if(null!=user_model) {
            Log.d(TAG, String.format("adapter position: %d Image url: %s", position, user_model.profile.picUrl));
        }
       if(user_model!=null && model.type == "PRIVATE") {
            if (user_model.profile.picUrl != null && !user_model.profile.picUrl.isEmpty() && user_model.profile.picUrl.contains("http")) {
                ImageLoader.getInstance().displayImage(user_model.profile.picUrl, holder.profilePic, ImageLoaderUtils.UIL_USER_AVATAR_DISPLAY_OPTIONS);
            }else{
                ImageLoader.getInstance().displayImage("", holder.profilePic, ImageLoaderUtils.UIL_USER_AVATAR_DISPLAY_OPTIONS);
            }

            if(!model.name.equals(user_model.profile.getName())) {
                model.setName(user_model.profile.getName());

                DbUtils.updateLocalDialogUser(dataManager, user_model.getProfile().getChat().getId(), user_model.getProfile().getName());
            }

        } else if (model.type == "GROUP") {
            if (model.getImageUrl() != null && !model.getImageUrl().isEmpty() && model.getImageUrl().contains("http")) {
                ImageLoader.getInstance().displayImage(model.getImageUrl(), holder.profilePic, ImageLoaderUtils.UIL_USER_AVATAR_DISPLAY_OPTIONS);
            }else{
                ImageLoader.getInstance().displayImage("", holder.profilePic, ImageLoaderUtils.UIL_USER_AVATAR_DISPLAY_OPTIONS);

            }
        } else {
            Bitmap image = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_placeholder);
            holder.profilePic.setImageBitmap(image);
        }
        holder.name.setText(model.name);

        if (model.getLastMessageDateSent() == 0) {
            holder.sendTime.setText(Constants.EMPTY_STRING);
        } else {
            holder.sendTime.setText(DateUtils.toTimeYesterdayFullMonthDate(model.getLastMessageDateSent()));
        }
        if (model.getUnreadMessagesCount() > 0) {
            holder.notification.setVisibility(View.VISIBLE);
            holder.notification.setText(String.valueOf(model.getUnreadMessagesCount()));
        } else
            holder.notification.setVisibility(View.GONE);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongPress.notify(model);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return dialogs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        MaskedImageView profilePic;
        TextView name, lastMessage, sendTime, notification;

        public ViewHolder(View view) {
            super(view);

            profilePic = (MaskedImageView) view.findViewById(R.id.image_user);
            name = (TextView) view.findViewById(R.id.txt_name);
            lastMessage = (TextView) view.findViewById(R.id.txt_last_message);
            sendTime = (TextView) view.findViewById(R.id.text_send_time);
            notification = (TextView) view.findViewById(R.id.text_notification);
        }
    }

    public void setNewData(List<DialogModel> newData) {
        dialogs = newData;
        notifyDataSetChanged();
    }

    public DialogModel getItem(int position) {
        return dialogs.get(position);
    }
}
